<?php

namespace Drupal\path_file\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\path_file\Entity\PathFileEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * An example controller.
 */
class PathFileController extends ControllerBase {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The entityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystem $file_system, EntityTypeManagerInterface $entity_type_manager) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('file_system'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function file(PathFileEntityInterface $path_file_entity) {
    $fid = $path_file_entity->getFid();
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    $uri = $file->getFileUri();
    $server_path = $this->fileSystem->realpath($uri);
    return new BinaryFileResponse($server_path);
  }

}
